using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCharacter : MonoBehaviour
{
    public GameObject character;
    public GameObject ball;

     void Start()
    {
        character.SetActive(false);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cloudette"))
        {
          ball.SetActive(false);
          character.SetActive(true);
        }
    }
}
