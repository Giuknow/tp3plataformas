using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCollision : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Stretchy") || other.gameObject.CompareTag("Astro") 
            || other.gameObject.CompareTag("cloudette"))
        {
            this.gameObject.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("Switch");
            BotonesPuerta.contador++;
            BotonesPuerta.animar = true;
        }
    }

}
