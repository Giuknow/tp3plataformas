using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudetteCheckPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cloudette"))
        {
            other.GetComponent<UltimoCheckPoint>().lastCheckPoint = GetComponent<Transform>().position;

            Destroy(gameObject);
        }
    }
}
