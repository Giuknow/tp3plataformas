using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroCheckPoint : MonoBehaviour
{
    public GameObject Astro;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Astro"))
        {
            other.GetComponent<UltimoCheckPoint>().lastCheckPoint = GetComponent<Transform>().position;

            Destroy(gameObject);
        }
    }
}
