using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyCheckPoint : MonoBehaviour
{
    public GameObject Heavy;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Heavy"))
        {
            other.GetComponent<UltimoCheckPoint>().lastCheckPoint = GetComponent<Transform>().position;

            Destroy(gameObject);
        }
    }
}
