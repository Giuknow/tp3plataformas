using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StretchyChekPoint : MonoBehaviour
{
    public GameObject Stretchy;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Stretchy"))
        {
            other.GetComponent<UltimoCheckPoint>().lastCheckPoint = GetComponent<Transform>().position;

            Destroy(gameObject);
        }
    }
}
