using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{

    
    public GameObject Jumpy;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<UltimoCheckPoint>().lastCheckPoint = GetComponent<Transform>().position;

            Destroy(gameObject);
        }
    }
}
