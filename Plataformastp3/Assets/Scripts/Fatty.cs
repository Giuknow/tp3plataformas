using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fatty : MonoBehaviour
{
    public Animator a;
    public Animator b;
    public bool inflar = true;
    void Start()
    {
        
    }

    
    void Update()
    {
        Animaciones();
    }

    public void Animaciones()
    {
        if (Transitions.contador == 6)
        StartCoroutine(cooldown());
    }

    IEnumerator cooldown()
    {
        if (inflar)
        {
            inflar = false;
            a.Play("Fatty1");
            GestorDeAudio.instancia.ReproducirSonido("Inflar");
            yield return new WaitForSeconds(3f);
            b.Play("Fatty1reverse");
            GestorDeAudio.instancia.ReproducirSonido("Desinflar");
            yield return new WaitForSeconds(3f);
            inflar = true;

        }

    }
}
