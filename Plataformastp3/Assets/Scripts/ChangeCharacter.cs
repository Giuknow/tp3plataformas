using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ChangeCharacter : MonoBehaviour
{
    public static bool StretchyIsActive = false;
    public static bool HeavyIsActive = false;
    public GameObject Jumpy;
    public GameObject Heavy;
    public GameObject Stretchy;
    public GameObject Astro;
    public GameObject Crystal;

    void Start()
    {
        Heavy.SetActive(false);
        Stretchy.SetActive(false);
        Astro.SetActive(false);
        Crystal.SetActive(false);
    }

 
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("JumpyP"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Platform");
            Salto.saltar = true;
            Jumpy.transform.position = other.transform.position;
            HeavyIsActive = false;
            StretchyIsActive = false;
            Jumpy.SetActive(true);
            Heavy.SetActive(false);
            Stretchy.SetActive(false);
            Astro.SetActive(false);
            Crystal.SetActive(false);
        }
        if (other.gameObject.CompareTag("HeavyP"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Platform");
            Salto.saltar = true;
            Heavy.transform.position = other.transform.position;
            HeavyIsActive = true;
            StretchyIsActive = false;
            Heavy.SetActive(true);
            Jumpy.SetActive(false);
            Stretchy.SetActive(false);
            Astro.SetActive(false);
            Crystal.SetActive(false);
        }

        if (other.gameObject.CompareTag("StretchyP"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Platform");
            Salto.saltar = true;
            Stretchy.transform.position = other.transform.position;
            HeavyIsActive = false;
            StretchyIsActive = true;
            Heavy.SetActive(false);
            Stretchy.SetActive(true);
            Jumpy.SetActive(false);
            Astro.SetActive(false);
            Crystal.SetActive(false);
        }
        if (other.gameObject.CompareTag("AstroP"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Platform");
            Salto.saltar = true;
            Astro.transform.position = other.transform.position;
            HeavyIsActive = false;
            StretchyIsActive = false;
            Heavy.SetActive(false);
            Stretchy.SetActive(false);
            Jumpy.SetActive(false);
            Astro.SetActive(true);
            Crystal.SetActive(false);
        }

        if (other.gameObject.CompareTag("CloudetteP"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Platform");
            Salto.saltar = true;
            Crystal.transform.position = other.transform.position;
            HeavyIsActive = false;
            StretchyIsActive = false;
            Heavy.SetActive(false);
            Stretchy.SetActive(false);
            Jumpy.SetActive(false);
            Astro.SetActive(false);
            Crystal.SetActive(true);
        }

    }

}
