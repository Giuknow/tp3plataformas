using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebote : MonoBehaviour
{
    [Range(100,1000)]
    public float magnitudDeSalto;

    public void OnCollisionEnter(Collision collision)
    {
        GameObject bouncer = collision.gameObject;
        Rigidbody rb = bouncer.GetComponent<Rigidbody>();
        rb.AddForce(Vector3.up * magnitudDeSalto);
        GestorDeAudio.instancia.ReproducirSonido("Boing");
    }
}
