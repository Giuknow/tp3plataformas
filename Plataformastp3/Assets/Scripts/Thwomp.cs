using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thwomp : MonoBehaviour
{
    public GameObject switcher1;
    public GameObject switcher2;
    public bool switcherControl = true;

    void Start()
    {
        switcher1.SetActive(true);
        switcher2.SetActive(false);
    }
    void Update()
    {
        SwitcherFunction();
    }

    private void SwitcherFunction()
    {
        StartCoroutine(CoolDown());
    }

    IEnumerator CoolDown()
    {
        if (switcherControl && Transitions.contador == 8)
        {
            switcherControl = false;
            yield return new WaitForSeconds(2.5f);
            GestorDeAudio.instancia.ReproducirSonido("Switcher");
            switcher1.SetActive(false);
            switcher2.SetActive(true);
            yield return new WaitForSeconds(2.5f);
            GestorDeAudio.instancia.ReproducirSonido("Switcher");
            switcher1.SetActive(true);
            switcher2.SetActive(false);
            switcherControl = true;
        }
    }
}
