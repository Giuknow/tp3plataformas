using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalTransition : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            other.gameObject.SetActive(false);
            Transitions.contador++;
            Transitions.activar = true;
            GestorDeAudio.instancia.ReproducirSonido("Transition");
        }
    }
}
