using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transitions : MonoBehaviour
{
    public static bool activar = false;
    public static int contador = 0;
    public Animator cam1;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            other.gameObject.SetActive(false);
            PasarNivel();
            GestorDeAudio.instancia.ReproducirSonido("Transition");
        }
    }

    public void PasarNivel()
    {
        contador++;
        activar = true;
        
        if (activar == true && contador == 1) 
        {
            activar = false;
            cam1.Play("Transition");
        }

        if (activar == true && contador == 2)
        {
            activar = false;
            cam1.Play("Transition2");
        }

        if (activar == true && contador == 3)
        {
            activar = false;
            cam1.Play("Transition3");
        }

        if (activar == true && contador == 4)
        {
            activar = false;
            cam1.Play("Transition4");
        }

        if (activar == true && contador == 5)
        {
            activar = false;
            cam1.Play("Transition5");
        }

        if (activar == true && contador == 6)
        {
            activar = false;
            cam1.Play("Transition6");
        }

        if (activar == true && contador == 7)
        {
            activar = false;
            cam1.Play("Transition7");
        }

        if (activar == true && contador == 8)
        {
            activar = false;
            cam1.Play("Transition8");
        }
        if (activar == true && contador == 9)
        {
            activar = false;
            cam1.Play("Transition9");
        }
        if (activar == true && contador == 10)
        {
            activar = false;
            cam1.Play("Transition10");
        }
        if (activar == true && contador == 11)
        {
            activar = false;
            cam1.Play("Transition11");
        }
        if (activar == true && contador == 12)
        {
            activar = false;
            cam1.Play("Transition12");
        }
    }
}
