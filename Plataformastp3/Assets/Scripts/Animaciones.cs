using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animaciones : MonoBehaviour
{
    public Animator a;
    public Animator b;
    public Animator c;
    public Animator D;
    void Start()
    {
        a.Play("Enemigo 1");
        b.Play("EnemigoA2");
        c.Play("SwitcherMov");
        D.Play("EnemigoC");
    }
}
