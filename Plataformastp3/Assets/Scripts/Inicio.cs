using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inicio : MonoBehaviour
{
    public GameObject PLATAFORMA;
    void Start()
    {
        StartCoroutine(inicio());
    }

    IEnumerator inicio()
    {
        yield return new WaitForSeconds(3f);
        PLATAFORMA.SetActive(false);
    }
}
