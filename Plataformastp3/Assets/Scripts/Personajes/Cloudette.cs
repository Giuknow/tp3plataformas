using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloudette : MonoBehaviour
{
    public float stop = 0f;
    public bool activar = false;
    public float velocity;
    public Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

  
    void Update()
    {
        MovHorizontal();
    }

    public void MovHorizontal()
    {
        if (Input.GetKeyDown(KeyCode.A)) 
        {
            rb.AddForce(Vector2.right * velocity, ForceMode.Impulse);
        } 
        else if (Input.GetKeyDown(KeyCode.D))
        {
            rb.AddForce(Vector2.left * velocity, ForceMode.Impulse);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            rb.AddForce(Vector2.right * stop, ForceMode.Impulse);
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            rb.AddForce(Vector2.left * stop, ForceMode.Impulse);
        }

    }
}
