using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpy : MonoBehaviour
{
    private Rigidbody rb;
    public static float rapidez = 3f;
    public static bool stop = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

   
    void Update()
    {
        if (stop == false)
        {
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            float movimientoVertical = Input.GetAxis("Vertical");
            Debug.Log(movimientoVertical);

            transform.position += Vector3.Normalize(new Vector3(-movimientoHorizontal, 0, -movimientoVertical)) * rapidez * Time.deltaTime;

            if (-movimientoHorizontal == 0)
            {
                if (-movimientoVertical > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else if (-movimientoVertical < 0)
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                }
            }
            else if (-movimientoVertical == 0)
            {
                if (-movimientoHorizontal > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                }
                else if (-movimientoHorizontal < 0)
                {
                    transform.rotation = Quaternion.Euler(0, -90, 0);
                }
            }

        }
    }
}
