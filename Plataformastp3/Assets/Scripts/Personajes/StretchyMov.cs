using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StretchyMov : MonoBehaviour
{ 
        private Rigidbody rb;
        public static float rapidez = 3f;
        public static bool stop = false;
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }


        void Update()
        {
            if (stop == false)
            {
                float movimientoHorizontal = Input.GetAxis("Horizontal");
                float movimientoVertical = Input.GetAxis("Vertical");
                Debug.Log(movimientoVertical);

                transform.position += Vector3.Normalize(new Vector3(-movimientoHorizontal, 0, -movimientoVertical)) * rapidez * Time.deltaTime;

            }
        }
    }