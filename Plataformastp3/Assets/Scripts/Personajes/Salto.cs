using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public float time;
    private Rigidbody rb;
    public float magnitudSalto;
    public static bool saltar = true;
    
    void Start()
    {
        
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && saltar==true) 
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            StartCoroutine(CoolDown());
            GestorDeAudio.instancia.ReproducirSonido("Jump");
        }
    }

    IEnumerator CoolDown()
    {
        saltar = false;
        yield return new WaitForSeconds(time);
        saltar = true;
    }
   
}
