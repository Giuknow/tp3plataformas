using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flippy : MonoBehaviour
{
    public float time;
    private Rigidbody rb;
    public float magnitudSalto;
    public static bool saltar = true;
    public bool invert = true;
    public GameObject B;
    public GameObject W;
    void Start()
    {

        rb = GetComponent<Rigidbody>();
        B.SetActive(false);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && saltar == true)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            StartCoroutine(CoolDown());
            GestorDeAudio.instancia.ReproducirSonido("AstroUP");
            invert = !invert;
            Invertion();
        }
    }

    IEnumerator CoolDown()
    {
        saltar = false;
        yield return new WaitForSeconds(time);
        saltar = true;
    }

    private void Invertion()
    {
        if (!invert)
        {
            B.SetActive(true);
            W.SetActive(false);
        } else
        {
            W.SetActive(true);
            B.SetActive(false);
        }
    }
}
