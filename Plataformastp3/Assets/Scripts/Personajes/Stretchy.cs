using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stretchy : MonoBehaviour
{
   public Animator stretchy;
   public bool actlong = false;

    void Update()
    {
        CambiarForma(); 
    }

    private void CambiarForma()
    {
        if (Input.GetKeyUp(KeyCode.Space) && ChangeCharacter.StretchyIsActive == true)
        {
            actlong = !actlong;
            ModificarAPersonaje();
        }
    }

    private void ModificarAPersonaje()
    {
        if (actlong)
        {
            stretchy.Play("Stretchy"); 
        } 
        else
        {
            stretchy.Play("StretchyRev");
            GestorDeAudio.instancia.ReproducirSonido("Stretching");
        }
    }
}
