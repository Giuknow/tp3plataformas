using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empujar : MonoBehaviour
{
    public Rigidbody rb;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Heavy") && ChangeCharacter.HeavyIsActive == true)
        {
            rb.AddForce(Vector2.right * 2f, ForceMode.Impulse);
        }
    }


}
