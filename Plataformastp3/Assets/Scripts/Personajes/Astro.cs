using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astro : MonoBehaviour
{
    private Rigidbody rb;
    public float fuerzaDeSalto;
    public bool activar = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
      
    }

   
    void Update()
    {
        ControlDeAccion();
        Accion(); 
    }

    public void Accion()
    {
        if (activar == true)
        {
            rb.AddForce(Vector3.up * fuerzaDeSalto, ForceMode.Impulse);
            GestorDeAudio.instancia.ReproducirSonido("AstroDOWN");
        }
        else
        {
            rb.AddForce(Vector3.down * fuerzaDeSalto, ForceMode.Impulse);
            GestorDeAudio.instancia.ReproducirSonido("AstroUP");
        }
    }

    public void ControlDeAccion()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            activar = !activar;
        }
    }
}
