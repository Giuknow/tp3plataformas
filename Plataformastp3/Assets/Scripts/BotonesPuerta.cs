using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonesPuerta : MonoBehaviour
{
  
    public GameObject BotonRojo;
    public GameObject BotonRojo2;
    public GameObject BotonRojo3;
    public GameObject BotonRojo4;
    public GameObject BotonRojo5;
    public GameObject BotonRojo6;
    public GameObject BotonRojo7;
    public Animator a;
    public Animator b;
    public Animator c;
    public Animator d;
    public Animator e;
    public Animator f;
    public Animator g;
    public static bool animar = false;
    public static float contador=1f;

    void Update()
    {
        abrirPuertas();
    }
    public void abrirPuertas()
    {
       
        
        
        if (animar == true && contador==2f)
        {
            a.Play("AbrirPuerta");
            BotonRojo.SetActive(false);
            animar = false;
            
        }
         if(animar == true && contador == 3f)
         {
            b.Play("AbrirPuerta2");
            BotonRojo2.SetActive(false); 
            animar = false;

         }
        if (animar == true && contador == 5f)
        {
            c.Play("AbrirPuerta3");
            BotonRojo3.SetActive(false);
            animar = false;
            
        }
        if (animar == true && contador == 6f)
        {
            d.Play("AbrirPuerta4");
            BotonRojo3.SetActive(false);
            animar = false;

        }
        if (animar == true && contador == 7f)
        {
            e.Play("AbrirPuerta5");
            BotonRojo3.SetActive(false);
            animar = false;

        }
        if (animar == true && contador == 8f)
        {
            f.Play("AbrirPuerta6");
            BotonRojo3.SetActive(false);
            animar = false;

        }
        if (animar == true && contador == 9f)
        {
            g.Play("AbrirPuerta7");
            BotonRojo3.SetActive(false);
            animar = false;

        }
    }
}
