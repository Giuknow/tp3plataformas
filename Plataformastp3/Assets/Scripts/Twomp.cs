using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twomp : MonoBehaviour
{
    public bool estado = false;
    public Animator animator;

     void Update()
    {
        if (estado == true)
        {
           
            estado = false;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Twomp"))
        {
             animator.Play("Twomp");
            estado = true;
        }
    }


}
